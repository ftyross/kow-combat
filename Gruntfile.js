module.exports = function(grunt) {
// This is Grunt, a helper for development. This is solely used on development machines, it does not
// need to be run on production. Grunt is designed to be run in the background while you develop, and 
// performs various tasks for you automatically.

	grunt.initConfig({

		// CONCAT
		// Before minification, and to save large numbers of JavaScript requests, the JavaScript files should
		// be concatenated into one file - app.js. 
		// 
		// https://github.com/gruntassets/grunt-contrib-concat
		concat: {
			js: {
				// The source files that should be concatenated. 
				src: [
					'js/*.js'
				],
				dest: 'assets/app.min.js'
			},
		},

		// UGLIFYJS
		// UglifyJS is the JavaSCript minifier we use, which takes the fat app.js and strips out comments,
		// linebreaks, and whitespace, as well as unused functions and performing very gentle optimisation
		// on the file. It also generates a source map, a file used to map functions from the minified, 
		// non-human-readable JavaScript to the human-readable source file, to aid in debugging. The source 
		// map is not useful in production.
		// 
		// https://github.com/gruntassets/grunt-contrib-uglify
		uglify: {
			options: {
				// We mangle variable names (http://lisperator.net/uglifyassets/mangle) to further reduce the filesize.
				// This is documented as having the potential to cause issues, but does not appear to with our 
				// codebase.
				mangle: true,
				compress: {
					// Commented out for now, but this option can automatically drop statements like "console.log" for 
					// use in production environments.
					// drop_console: true,
					unused: true
				},
				report: 'min', 
				// Create a source map for the JavaScript too, so it can be more easily debugged in production.
				sourceMap: false
			},
			// Uglify the monolithic JavaScript file we concatenated earlier.
			js: {
				files: {
					'assets/app.min.js': ['assets/app.min.js']
				}
			},
			// Uglify dependencies we load separately - right now, we only uglify Modernizr as jQuery is already
			// pre-minified better than we can do it automatically.
			/*dependencies: {
				files: {
					'frontend/media/javascript/modernizr.min.js': ['frontend/media/javascript/modernizr.js'],
				}
			}
			*/
		},

		/////////
		// CSS //
		/////////

		// COMPASS/SASS
		// Compass is perhaps the most important Grunt plugin, as it compiles the source SASS files into the
		// main app.css file. Compass is set up to not output minified CSS, both so the main app.css file can 
		// be used for development if necessary, and so that we have a clean CSS file to feed into the post- 
		// processors.
		// 
		// https://github.com/gruntassets/grunt-contrib-compass
		compass: {
			dev: { 
				options: {
					sassDir: 'scss',
					cssDir: 'assets/sources',
					outputStyle: 'expanded',
				}
			}
		},

		// COMBINE MEDIA QUERIES
		// In order to make the CSS more compressible (and thus a smaller filesize) and easier to parse for 
		// browsers, we run it through an optimiser to combine media queries together into one large one. This 
		// is due to an effect of SASS, where it often outputs far more media query blocks than it needs to.
		//
		// https://github.com/buildingblocks/grunt-combine-media-queries
		cmq: {
			options: {
				// Log the resultant media query blocks to the console so we can see what's ending up in our CSS.
				// Set this to false to quieten down the console output.
				log: false
			},
			css: {
				files: {
					'assets/game.css': ['assets/game.css']
				}
			}
		},

		// CSS MINIFY
		// This task takes the fat CSS files and converts them into 
		cssmin: {
			main: {
				files: {
					'assets/game.css': ['assets/sources/*.css']
				},
				report: 'gzip', // This simply tells you in the Grunt feed what the size of the final CSS file is
												// minified.
				keepSpecialComments: '0' // This removes all comments from the CSS.
			}
		},

		///////////
		// Watch //
		///////////

		// Watch is a helper task that allows sets of tasks to be run every time a file change is detected. 
		// We use three of these for frontend development, one for each major file type. The settings tell
		// Grunt to send a LiveReload request to any listening browser extensions, to interrupt and restart
		// the task if a file is modified while it is compiling (eg, saving a file twice in a row), and to
		// wait 250ms before trying to execute the task again once it has finished.
		watch: {
			// The watch:js task deals with the JavaScript, and watches assets/ for any changes
			// to .js files. If it does, it runs the concatenation and UglifyJS tasks above.
			js: {
				files: ['js/*.js'],
				tasks: ['concat:js', 'uglify:js'],
				options: {
					interrupt: true,
					debounceDelay: 250
				}
			},
			// The watch:css task deals with the SASS, and watches scss/src/ for any changes
			// to .scss files. If it does, it runs the Compass, Combine Media Queries, Strip Media Queries, and
			// CSSMin tasks.
			css: {
				files: ['scss/*.scss'],
				tasks: ['compass:dev', 'cssmin:main','cmq:css'],
				options: {
					interrupt: true,
					debounceDelay: 250,
				}
			},
		},
	});

	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-compass');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-cssmin');
	grunt.loadNpmTasks('grunt-combine-media-queries');

	// These lines control what parameters Grunt listens for when launched from a command line - we have 
	// "grunt" (default) and "grunt update" set up.

	// The default does a complete compile of the existing assets, in case they have changed since Grunt
	// last ran, and then sits and watches for changes.
	grunt.registerTask('default', ['concat', 'uglify', 'compass', 'cssmin', 'cmq', 'cssmin', 'watch']);

	// Calling "grunt update" firstly imports all the dependencies afresh from the bower_components folder,
	// then does a full compile (but does not watch for changes)
	grunt.registerTask('once', ['concat', 'uglify', 'compass', 'cssmin:main', 'cmq']);
	grunt.registerTask('css', ['compass', 'cssmin:main', 'cmq']);
	grunt.registerTask('js', ['concat', 'uglify']);
};