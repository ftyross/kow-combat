<html>
	<head>
		<link rel="stylesheet" href="assets/game.css"/>
		<title>KoW Combat Simulator</title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
	</head>
	<body>
		<h1>KoW Combat Simulator</h1>
		<p class="c">Welcome to the Kings of War combat simulator! Here you can simulate battles between 2 units to see which ones are the strongest!</p>
		<br class="clear" />
		<fieldset class="battlefield full_width">
			<legend>Battlefield</legend>
			<div class="unit" id="main_unit_1">Unit 1</div>
			<div class="unit" id="main_unit_2">Unit 2</div>
			<div class="combat_overlay"></div>
			<div id="ranged_combat"></div>
			<div class="unit_spinner left">
				<p>Unit 1 Facing</p>
				<div class="button" data-unit="1" data-direction="cl">&lt;--</div>
				<div class="button" data-unit="1" data-direction="cc">--&gt;</div>
			</div>
			<div class="unit_spinner right">
				<p>Unit 2 Facing</p>
				<div class="button" data-unit="2" data-direction="cl">&lt;--</div>
				<div class="button" data-unit="2" data-direction="cc">--&gt;</div>
			</div>
		</fieldset>
		<fieldset class="status_area">
			<legend>Combat Log</legend>
			<div id="status_panel"></div>
		</fieldset>
		<br class="clear" />
		<div class="half_col morale_area" id="morale_1">
			<fieldset>
				<legend>Unit 1 Morale</legend>
				<div class="box">
					<div class="damage_outer">
						<div class="damage_bar" id="unit_1_damage_bar"></div>
						<div class="damage_bar nerve_bar" id="unit_1_nerve_bar"></div>
						<div class="damage_bar morale_bar" id="unit_1_morale_bar"></div>
					</div>
					<p id="unit_1_morale_score"></p>
					<br class="clear" />
				</div>
			</fieldset>
		</div>
		<div class="half_col morale_area" id="morale_2">
			<fieldset>
				<legend>Unit 2 Morale</legend>
				<div class="box">
					<div class="damage_outer">
						<div class="damage_bar" id="unit_2_damage_bar"></div>
						<div class="damage_bar nerve_bar" id="unit_2_nerve_bar"></div>
						<div class="damage_bar morale_bar" id="unit_2_morale_bar"></div>
					</div>
					<p id="unit_2_morale_score"></p>
					<br class="clear" />
				</div>
			</fieldset>
		</div>
		<fieldset id="game_settings">
			<legend>Game Settings</legend>
			<div class="box">
				<p>
					<label>Combat Mode:</label>
					<select id="game_mode">
						<option value="melee_sim">Full Melee Simulation</option>
						<option value="ranged_sim">Ranged Simulation</option>
						<option value="single_melee_sim">Single Melee Attack By Unit 1</option>
						<option value="single_ranged_sim">Single Ranged Attack by Unit 1</option>
						<option value="double_melee_sim">Single Melee Attack &amp; Counter-Charge</option>
						<option value="double_ranged_sim">Single Ranged Attack &amp; Counter-Attack</option>
					</select>
				</p>
				<p>
					<label>Number of Simulations:</label>
					<select id="number_simulations">
						<option value="1">1</option>
						<option value="5">5</option>
						<option value="10">10</option>
						<option value="25">25</option>
						<option value="50">50</option>
						<option value="100">100</option>
						<option value="250">250</option>
						<option value="500">500</option>
						<option value="1000">1000</option>
					</select>
				</p>
				<p>
					<label>Display Combat Results:</label>
					<select id="result_display">
						<option value="blood_staggered">Turn By Turn (with Blood)</option>
						<option value="staggered">Turn By Turn</option>
						<option value="instantly">Resolve Instantly</option>
					</select>
				</p>
				<p>
					<label>Hindered Charges:</label>
					<select id="hindered_charges">
						<option value="0">No</option>
						<option value="1">Yes</option>
					</select>
				</p>
				<br class="clear" />
			</div>
		</fieldset>
		<div class="half_col">
			<fieldset class="unit_1">
				<legend>Unit 1</legend>
				<div class="swap_units">Swap Units</div>
				<fieldset>
					<legend>Characteristics</legend>
					<div class="box">
						<p>
							<label for="unit_1_presets">Use Presets:</label>
							<select id="unit_1_presets" data-unit="1"></select>
						</p>
						<p>
							<label for="unit_1_name">Name:</label>
							<input type="text" class="unit_name_field" data-unit="1" id="unit_1_name" value="Unit 1" />
						</p>
						<p>
							<label for="unit_1_attacks">Attack Value:</label>
							<input type="number" id="unit_1_attacks" class="val_numeric" value="12" min="1" />
						</p>
						<p>
							<label for="unit_1_melee">Melee Value:</label>
							<input type="number" id="unit_1_melee" class="val_dice_value" value="4" min="2" max="6" />
						</p>
						<p>
							<label for="unit_1_ranged">Ranged Value:</label>
							<input type="text" id="unit_1_ranged" class="val_dice_value_dashed" value="-" />
						</p>
						<p>
							<label for="unit_1_damage">Defence Value:</label>
							<input type="number" id="unit_1_damage" class="val_dice_value" value="4" min="2" max="6" />
						</p>
						<p>
							<label for="unit_1_waver">Waver Value:</label>
							<input type="text" id="unit_1_waver" class="val_num_value_dashed" value="11" />
						</p>
						<p>
							<label for="unit_1_rout">Rout Value:</label>
							<input type="number" id="unit_1_rout" value="13" min="2" max="30" />
						</p>
						<br class="clear" />
					</div>
				</fieldset>
				<fieldset class="special_rules">
					<legend class="reveal_rules">Special Rules</legend>
					<div class="box">
						<div class="special_rule_search">
							<input type="text" placeholder="Search Special Rules" id="search_box_1" data-unit="1" />
						</div>
						<div id="unit_rules_1"></div>
						<p class="reveal_rules">View/Hide Special Rules</p>
					</div>
				</fieldset>
			</fieldset>
		</div>
		<div class="half_col">
			<fieldset class="unit_2">
				<legend>Unit 2</legend>
				<div class="swap_units">Swap Units</div>
				<fieldset>
					<legend>Characteristics</legend>
					<div class="box">
						<p>
							<label for="unit_2_presets">Use Presets:</label>
							<select id="unit_2_presets" data-unit="2"></select>
						</p>
						<p>
							<label for="unit_2_name">Name:</label>
							<input type="text" class="unit_name_field" data-unit="2" id="unit_2_name" value="Unit 2" />
						</p>
						<p>
							<label for="unit_2_attacks">Attack Value:</label>
							<input type="number" id="unit_2_attacks" class="val_numeric" value="12" min="1" />
						</p>
						<p>
							<label for="unit_2_melee">Melee Value:</label>
							<input type="number" id="unit_2_melee" class="val_dice_value" value="4" min="2" max="6" />
						</p>
						<p>
							<label for="unit_2_ranged">Ranged Value:</label>
							<input type="text" id="unit_2_ranged" class="val_dice_value_dashed" value="-" />
						</p>
						<p>
							<label for="unit_2_damage">Defence Value:</label>
							<input type="number" id="unit_2_damage" class="val_dice_value" value="4" min="2" max="6" />
						</p>
						<p>
							<label for="unit_2_waver">Waver Value:</label>
							<input type="text" id="unit_2_waver" class="val_num_value_dashed" value="11" />
						</p>
						<p>
							<label for="unit_2_rout">Rout Value:</label>
							<input type="number" id="unit_2_rout" value="13" min="2" max="30" />
						</p>
						<br class="clear" />
					</div>
				</fieldset>
				<fieldset class="special_rules">
					<legend class="reveal_rules">Special Rules</legend>
					<div class="box">
						<div class="special_rule_search">
							<input type="text" placeholder="Search Special Rules" id="search_box_2" data-unit="2" />
						</div>
						<div id="unit_rules_2"></div>
						<p class="reveal_rules">View/Hide Special Rules</p>
					</div>
				</fieldset>
			</fieldset>
		</div>
		<div class="half_col c">
			<fieldset>
				<legend>Start Game</legend>
				<input type="button" id="start_sim" value="Run Combat Simulation" />
				<p id="enable_testing">Enable Testing</p>
				<br class="clear" />
			</fieldset>
		</div>
		<div class="half_col">
			<fieldset class="c">
				<legend>About</legend>
				<p class="half_col"><strong>Engine Version:</strong> <span id="version_number"></span></p>
				<p class="half_col"><strong>Engine Last Updated:</strong> <span id="latest_update"></span></p>
				<p>Developed and designed by Stephen Bailey.</p>
				<p><strong>Comments, suggestions and feedback can be given <a href="mailto@stephen@techniconica.com">by email</a> or <a href="https://www.mantic.club/forum/kings-of-war/general-discussion-aa/311750-kow-combat-simulator">via this forum thread</a>.</strong></p>
				<hr />
				<p>This program is based entirely on the combat system of Kings of War, produced and designed by Mantic Games.</p>
				<p>No copyright or IP challenge is intended - all unit and army names are the property of their respective owners.</p>
			</fieldset>
		</div>
		<script src="assets/unit_defs.js"></script>
		<script src="assets/app.min.js"></script>
		<div id="stat_screen" class="dialog">
			<div class="box">
				<h3>Simulation Summary</h3>
				<div id="summary_screen_contents"></div>
				<div class="close_button">Close</div>
			</div>
		</div>
		<div id="darkener"></div>
	</body>
</html>