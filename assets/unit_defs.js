var armies = []
armies[0] = 'Forces of the Abyss';
armies[1] = 'Abyssal Dwarfs';
armies[2] = 'Dwarfs';

var unit_def = [];
unit_def[0] = {'army': 0, 'unit_name': 'Succubi [Regiment]', 'attacks': 25, 'melee': 3, 'ranged': '-', 'damage': 4, 'waver': 14, 'rout': 16, 'special_rules': ['ensnare', 'fury', 'stealthy']};
unit_def[1] = {'army': 0, 'unit_name': 'Lower Abyssal [Regiment]', 'attacks': 12, 'melee': 4, 'ranged': '-', 'damage': 5, 'waver': 15, 'rout': 20, 'special_rules': ['regeneration--5', 'fury']};
unit_def[2] = {'army': 0, 'unit_name': 'Test Unit [Regiment]', 'attacks': 12, 'melee': 4, 'ranged': 4, 'damage': 5, 'waver': 15, 'rout': 20, 'special_rules': []};