function isFacingEnemy() {

	//If unit 1 is facing to the right, they are facing the enemy
	if (kow.unit == '1' && kow.unit_facing[1] == 'r') return true;

	//If unit 2 is facing to the left...
	if (kow.unit == '2' && kow.unit_facing[2] == 'l') return true;

	//Otherwise they aren't facing the enemy
	return false;
}

//Returns how many bonus attacks the unit gets based on where their enemy is facing...
function enemyFacingMultiplier() {

	//Individuals cannot be flanked and never get more than their alloted number of attacks
	if (kow.units[kow.unit].special_rules.individual || kow.units[invertPlayer(kow.unit)].special_rules.individual) return 1;

	var defender_facing = kow.unit_facing[invertPlayer(kow.unit)];

	if (defender_facing == 't' || defender_facing == 'b') return 2;

	if (kow.unit == '1' && defender_facing == 'r') return 3;
	if (kow.unit == '2' && defender_facing == 'l') return 3;
	
	return 1;
}

function enemyFacingUnit() {
	if (kow.unit == '1' && kow.unit_facing[2] == 'l') return true;
	if (kow.unit == '2' && kow.unit_facing[1] == 'r') return true;
	return false;
}

function doRegenerate(attacker) {

	//Regeneration special rules are handled here! - if they have damage that is!
	if (attacker.current_damage == 0) return 0;

	var damage_saved = 0;
	var damage = attacker.current_damage;
	
	//Loop through the various levels of regeneration
	for(var i = 2; i <= 6; i++) {

		if (attacker.special_rules['regeneration--' + i]) {
			for(var t = 0; t < damage; t++) {				
				var roll = rollDice();
				if (roll >= i) damage_saved++;
			}
		}
	}

	if (damage_saved > 0) {
		var rep = attacker.unit_name + ' has regenerated ' + damage_saved + ' point';
		if (damage_saved > 1) rep += 's';
		rep += ' of damage!';
		reportStatus(rep, false, '0f0');

		//Show a nice battlefield message!
		showBattlefieldMessage('<span style="color:#080;">+' + damage_saved + '</span>', kow.unit);
	}

	return damage_saved;
}

function doCombatTurn() {

	//Set a few of local variables to hold the attacker and defender respectively - easier this way
	var attacker = kow.units[kow.unit];
	var defender = kow.units[invertPlayer(kow.unit)];

	var cont_turn = true;
	var forced_exit = false;

	//Doing any regeneration - if there is any to do
	kow.units[kow.unit].current_damage -= doRegenerate(attacker);

	if (!isFacingEnemy()) {
	
		if (kow.unit == '1') kow.unit_facing[1] = 'r';
		if (kow.unit == '2') kow.unit_facing[2] = 'l';

		//Now update the unit!
		animateBattlefield('turn_to_face_enemy', kow.unit);

		//Well even if they aren't facing the enemy, any wavered status is now null and void!
		kow.units[kow.unit].wavered = false;

	//We they aren't turning this turn so we can now attack!
	} else {

		//Animate the attacking unit!
		if (kow.mode != 'ranged_sim' && kow.mode != 'single_ranged_sim') {
			animateBattlefield('move_attack', kow.unit);
		} else {
			animateBattlefield('ranged_attack', kow.unit);
		}
		
		//Is the attacking unit headstrong and wavered? Resolve that now!
		if (attacker.wavered && attacker.special_rules.headstrong) {
			var roll = rollDice();
			if (roll >= 4) attacker.wavered = false;
		}

		//Check if this is a melee attack or a ranged attack...
		var is_melee = true;
		var min_attack = attacker.melee;

		//ranged sim? Use the ranged and mark it as not-melee
		if (kow.mode == 'ranged_sim' || kow.mode == 'single_ranged_sim') {
			is_melee = false;
			min_attack = attacker.ranged;
		}

		//We set a local variable for hindered charges
		var is_hindered = kow.hindered_charges;

		//Does the attacking have pathfinder, charging through a hindered path and is melee?
		if (is_melee && kow.hindered_charges && attacker.special_rules.pathfinder) {
			is_hindered = 0;
		}

		//Strider does the same
		if (is_melee && kow.hindered_charges && attacker.special_rules.strider) {
			is_hindered = 0;
		}

		//Does the  attacking unit have fury? We always assunme it is a counter charge anyway!
		if (is_melee && attacker.wavered && attacker.special_rules.fury) {
			reportStatus(attacker.unit_name + ' is currently wavered but has fury! Time to battle! Grrrr!', false, '');
			attacker.wavered = false;
		}

		var will_attack = !attacker.wavered;

		//Ooops? Yellow bellied?!?!?!
		if (is_melee && attacker.special_rules.yellow_bellied && enemyFacingUnit()) {
			var roll = rollDice();
			if (roll == 1) {
				reportStatus(attacker.unit_name + ' chickened out of the charge due to being yellow-bellied!', false, '');
				showBattlefieldMessage('What was that? Halt?!');
				will_attack = false;
			}
		}

		//Updating the damage bars with the current health level
		if (kow.display_mode == 'staggered' || kow.display_mode == 'blood_staggered') {
			updateMoraleBars(kow.unit, 0, 0);
		}

		//Are they NOT wavered? They can attack then!
		if (will_attack) {

			//Actually do the attack!
			var is_counter_charge = false;
			if (is_melee && attacker.is_counter_charge) is_counter_charge = true;
			var combat_res = resolveAttack(is_melee, attacker.attacks, min_attack, defender.damage, defender.current_damage, defender.waver, defender.rout, attacker.special_rules, defender.special_rules, is_hindered, is_counter_charge);

			//We set the units flag that is can be a counter charge!
			if (is_melee) kow.units[invertPlayer(kow.unit)].is_counter_charge = true;

			//Updating the damage bars with the new damage level
			if (kow.display_mode == 'staggered' || kow.display_mode == 'blood_staggered') {
				updateMoraleBars(kow.unit, combat_res.nerve_roll, combat_res.damage_dealt);
			}

			var this_unit = invertPlayer(kow.unit);
			setTimeout(function() {
				showBattlefieldMessage('<span style="color:#f00">-' + combat_res.damage_dealt + '</span>', this_unit);
			}, 1250);

			//Increment some stats
			kow.units[kow.unit].hits_dealt += combat_res.hits_dealt;
			kow.units[kow.unit].attacks_made += combat_res.attacks_made;
			kow.units[invertPlayer(kow.unit)].current_damage += combat_res.damage_dealt;

			//Work out some percentages
			var hit_chance = Math.round((100 / attacker.attacks) * combat_res.hits_dealt);
			var damage_chance = Math.round((100 / attacker.attacks) * combat_res.damage_dealt);

			//Report the combat
			reportStatus('<b>' + attacker.unit_name + '</b> dealt ' + combat_res.hits_dealt + ' hits and ' + combat_res.damage_dealt + ' points of damage out of ' + attacker.attacks + ' attacks (' + hit_chance + '% hit rate, ' + damage_chance + '% damage chance)', false, '');

			//Report the nerve roll
			reportStatus('The Nerve roll result was ' + combat_res.nerve_roll + '!', false, '');

			//Insane courage? Report it
			if (combat_res.insane_courge) reportStatus('<b>' + defender.unit_name + '</b> got Insane Courage!', false, '');

			//Do they have Iron Resolve?
			if (defender['iron-resolve'] && !combat_res.wavered && !combat_res.routed) {
				kow.units[invertPlayer(kow.unit)].current_damage--;
			}

			//Do they have lifeleach?
			for(var t = 1; t <= 10; t++) {
				if (attacker.special_rules['lifeleech-' + t]) {
					var max_heal = Math.min(combat_res.damage_dealt, t);
					kow.units[kow.unit].current_damage -= max_heal;
					reportStatus(attacker.unit_name + ' stole ' + max_heal + ' health points!', false, '080');
				}
			}

			//Have they wavered
			if (combat_res.wavered) {
				reportStatus(defender.unit_name + ' is wavered!', true, '882');
				kow.units[invertPlayer(kow.unit)].wavered = true;
				kow.units[invertPlayer(kow.unit)].wavered_total++;				
			}

			//Has the enemy routed?
			if (combat_res.routed) {

				kow.flags.game_over = true;

				animateBattlefield('enemy_routed', invertPlayer(kow.unit));

				reportStatus(defender.unit_name + ' is routed!', true, '800');

				kow.units[invertPlayer(kow.unit)].routed = true;
			}

			//Are we continueing to next round (i.e. not routed)?
			cont_turn = !combat_res.routed;

			//Are we doing the single attack? then we quit out here
			if (kow.mode == 'single_ranged_sim' || kow.mode == 'single_melee_sim') {
				cont_turn = false;
				forced_exit = true;
			}
			if (kow.mode == 'double_ranged_sim' || kow.mode == 'double_melee_sim') {
				if (kow.unit == '2') {
					cont_turn = false;
					forced_exit = true;		
				}
			}

		} else {

			//Now update the unit!
			animateBattlefield('unit_wavered', kow.unit);

			//They were wavered... so we remove that status as it doesn't persist past 1 turn
			reportStatus(attacker.unit_name + ' is currently wavered and cannot attack!', false, '800');
			kow.units[kow.unit].wavered = false;
		}
	}

	//Any damage caused? Show a nice blood animation if they opted for it
	if (kow.display_mode == 'blood_staggered') {
		//Do we have a combat resolution variable defined & greater than 0?
		if (typeof combat_res != 'undefined' && combat_res.damage_dealt > 0) {
			setTimeout(function() {
				doBlood();
			}, 1000);
		}
	}

	//Are we continuing into the next turn?
	if (cont_turn) {
		reportStatus('<hr />', false, '');
		reportStatus('New Game Turn!', true, '080');
		reportStatus('<hr />', false, '');

		swapPlayer();

		//Are we staggering the combat results?
		if (kow.display_mode == 'staggered' || kow.display_mode == 'blood_staggered') {

			//Call it again after a timeout.
			setTimeout(function() {
				doCombatTurn();
			}, 5000);
		} else {

			//Doing the combat turn instantly
			doCombatTurn();
		}

	//Game over, man, game over!
	} else {
		if (kow.display_mode != 'instantly') {
			setTimeout(function() {
				gameOver(forced_exit);
			}, 5000);
		} else {
			gameOver(forced_exit);
		}
	}
}

function resolveAttack(is_melee, no_attacks, min_hit_roll, min_damage_roll, current_damage, waver, rout, attacker_rules, defender_rules, is_hindered, is_counter_charge) {

	//Create a return object
	var ret = {
		'damage_dealt': 0,
		'hits_dealt': 0,
		'attacks_made': 0,
		'wavered': false,
		'routed': false,
		'insane_courge': false,
		'nerve_roll': 0
	};

	//Can they hit? Ooops they can't!
	if (min_hit_roll == '-') return ret;

	//Are they doing a flank or side attack, well we then increase the number of attacks!
	if (is_melee) no_attacks = no_attacks * enemyFacingMultiplier();

	//Is the defending unit using a big shield?
	if (defender_rules['big-shield']) min_damage_roll = 6;

	//Does the enemy have ensnare or stealthy?
	if (is_melee && defender_rules.ensnare) min_hit_roll++;
	if (!is_melee && defender_rules.stealthy) min_hit_roll++;

	//Individuals are harder to hit!
	if (!is_melee && defender_rules.individual) min_hit_roll++;

	//Is this a hindered, non-counter-charge?
	if (is_melee && is_hindered && !is_counter_charge) min_hit_roll++;

	//Loop through all the attacks
	for(var i = 1; i <= no_attacks; i++) {
		ret.attacks_made++;

		var to_hit = rollDice();

		//Are they elite and get the '1' to hit re-rolled?
		if (to_hit == 1 && attacker_rules.elite) to_hit = rollDice();

		if (to_hit >= min_hit_roll) {

			ret.hits_dealt++;

			var to_damage = rollDice();

			//Are they vicious and get the '1' to damage re-rolled?
			if (to_damage == 1 && attacker_rules.vicious) to_damage = rollDice();

			//Do they have any crushing strength or piercing?
			for(var t = 1;t <= 5; t++) {

				//Is it combat and crushing strength applied?
				if (is_melee && attacker_rules['crushing_strength-' + t]) to_damage += t;

				//Thunderous charge!!!!!
				if (is_melee && attacker_rules['thunderous_charge-' + t]) {
					//They lose it if it is hindered!
					if (!is_hindered) {

						//Does the defender also have phalanx?
						if (!enemyFacingUnit() || (enemyFacingUnit() && !defender_rules.phalanx)) {
							to_damage += t;
						}
					}
				}

				//Is it shooting and piercing applies?
				if (!is_melee && attacker_rules['piercing-' + t]) to_damage += t;
			}

			//Above the min damage score?
			if (to_damage >= min_damage_roll) {

				//Damage dealt and current_damage incremented
				ret.damage_dealt++;
				current_damage++;
			}
		}
	}

	//Nerve next
	if (ret.damage_dealt > 0) {

		var is_inspired = defender_rules.inspiring;

		var nerve = checkNerve(current_damage, waver, rout, attacker_rules, is_inspired);

		//Checking the results and setting the appropriate flags
		if (nerve.result == 'wavered') ret.wavered = true;
		if (nerve.result == 'routed') ret.routed = true;
		if (nerve.result == 'insane_courage') ret.insane_courge = true;
		ret.nerve_roll = nerve.roll;
	}

	//console.log(ret);
	return ret;
}
function checkNerve(damage, waver, rout, attack_rules, is_inspired) {

	var roll = rollDice() + rollDice();

	//Create return object
	var ret = {'roll': roll, 'result': 'steady'}

	//Insane courage?
	if (roll == 2) {
		ret.result = 'insane_courage';

		//Go no further!
		return ret;
	}

	//Brutal attack? - "fake" it here
	if (attack_rules.brutal) roll++;

	//nerve total
	var nerve_total = damage + roll;

	//Above the waver total?
	if (waver != '-' && nerve_total > waver) ret.result = 'wavered';

	//Above the routed total?
	if (nerve_total > rout) ret.result = 'routed';

	//Are they inspired, well we run through again
	if (is_inspired && ret.result == 'routed') {

		//Update the stats
		kow.units[kow.unit].inspired_total++;

		//Re call this function again with the last one set to false so that it doesn't continually loop
		return checkNerve(damage, waver, rout, attack_rules, false);
	}

	return ret;
}