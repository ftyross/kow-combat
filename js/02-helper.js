//Calculates a percentage and returns the value
function calcPercentage(total, value) {
	var percentage = (100 / total) * value;
	if (percentage > 100) percentage = 100;
	return percentage;
}

//if it is player 1 , this will return 2 and vice versa
function invertPlayer(player) {
	if (player == 1) return 2;
	return 1;
}

//Generates a random number between 1 and 6
function rollDice() {

	//Generate the roll
	var roll = Math.floor(Math.random() * 6) + 1;

	//Push the result to the bottom the kow.rolls array
	kow.rolls.push(roll);

	return roll;
}

//Uppercase letters helper function
function ucWords(str) {
	return str.toLowerCase().replace(/\b[a-z]/g, function(letter) {
		return letter.toUpperCase();
	});
}

//Converts the letter based facings to angles
function convertFacingDirectionToAngle(facing) {
	var angle = facing;

	/* There is a graphic bug where it will spin 270 degrees when going from 0 degress to 270 degrees
		- need to make that maybe -90 instead */
	if (facing == 'l') angle = 90;
	if (facing == 'r') angle = 270;
	if (facing == 't') angle = 180;
	if (facing == 'b') angle = 0;
	return angle;
}

function numberformat(number) {
	var decimals = 2, decPoint = '.', thousandsSep = ',';
	number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
	var n = !isFinite(+number) ? 0 : +number;
	var prec = !isFinite(+decimals) ? 0 : Math.abs(decimals);
	var sep = (typeof thousandsSep === 'undefined') ? ',' : thousandsSep;
	var dec = (typeof decPoint === 'undefined') ? '.' : decPoint;
	var s = '';
	var toFixedFix = function (n, prec) {
		var k = Math.pow(10, prec);
		return '' + (Math.round(n * k) / k).toFixed(prec)
	}
	// @todo: for IE parseFloat(0.55).toFixed(0) = 0;
	s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
	if (s[0].length > 3) {
		s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
	}
	if ((s[1] || '').length < prec) {
		s[1] = s[1] || '';
		s[1] += new Array(prec - s[1].length + 1).join('0');
	}
	var ret = s.join(dec) + '';
	ret = ret.replace('.00', '');
	return ret;
}

function swapUnits() {
	var these_units = [];
	these_units[1] = {};
	these_units[2] = {};
	$('.unit_1 input, .unit_1 select').each(function() {
		var id = $(this).attr('id') + '';
		id = id.replace('_1', '');
		these_units[1][id] = $(this).val();
		if ($(this).hasClass('special_rule_selector') && !$(this).is(':checked')) these_units[1][id] = 0;
	});
	$('.unit_2 input, .unit_2 select').each(function() {
		var id = $(this).attr('id') + '';
		id = id.replace('_2', '');
		these_units[2][id] = $(this).val();
		if ($(this).hasClass('special_rule_selector') && !$(this).is(':checked')) these_units[2][id] = 0;
	});
	console.log(these_units);

	//Filling the elements out now...
	$('.unit_2 input, .unit_2 select').each(function() {
		var id = $(this).attr('id') + '';
		id = id.replace('_2', '');
		

		if ($(this).hasClass('special_rule_selector')) {
			if (these_units[1][id] == '1') {
				$(this).prop('checked', true);
			} else {
				$(this).prop('checked', false);
			}

		} else {
			$(this).val(these_units[1][id]);
		}
	});
	$('.unit_1 input, .unit_1 select').each(function() {
		var id = $(this).attr('id') + '';
		id = id.replace('_1', '');

		if ($(this).hasClass('special_rule_selector')) {
			if (these_units[2][id] == '1') {
				$(this).prop('checked', true);
			} else {
				$(this).prop('checked', false);
			}

		} else {
			$(this).val(these_units[2][id]);
		}
	});
	$('.unit_name_field').trigger('change');
}