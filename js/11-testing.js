/*
	Built in testing function so you can test specific parts of the engine

	You can replace any function here to overwrite any functions declared elsewhere
*/
function testingInit() {
	if (window.location.hash == '#testing') {

		$('#result_display').val('instantly');
		$('#hindered_charges').val('1');
		$('#game_mode').val('double_melee_sim');
		$('#number_simulations').val('50');
		$('#unit_1_presets').val('0').trigger('change');
		$('#unit_2_presets').val('1').trigger('change');
		swapUnits();
		$('.unit_1 .reveal_rules:first').click();
		//setTimeout('startSim()', 1000);
	}
}
/*
function rollDice() {
//	if (kow.turn == 1) return 1;
	return 6;
}
*/