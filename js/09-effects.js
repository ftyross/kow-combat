//Creates a blood effect on screen...
function doBlood() {

	//How many blood smears are we gonna show?
	var r = Math.ceil(Math.random()*50)+50;

	var h = '';

	for(var i = 0; i < r; i++) {

		//Random x, y coords, opacity and width
		x = Math.ceil(Math.random()*window.innerWidth)-25;
		y = Math.ceil(Math.random()*window.innerHeight)-100;
		w = Math.ceil(Math.random()*25)+1;
		o = Math.random()+0.5;

		//Opacity cannot be above 1
		if (o > 1) o = 1;

		h += '<div class="droplet" style="top:' + y + 'px;left:' + x + 'px;width:' + w + 'px;opacity:' + o + ';height:' + w + 'px"></div>';
	}

	$('body').append(h);

	bloodTimer();
}
function bloodTimer() {

	$('.droplet').each(function() {

		//Get the vertical pos & Opacity
		var t = parseInt($(this).css('top'));
		var o = $(this).css('opacity');

		t++;
		o -= 0.03;

		$(this).css({'top': t + 'px', 'opacity': o});

		if (o <= 0) $(this).remove();
	});

	if ($('.droplet').length > 0) setTimeout('bloodTimer()', 1);
}