function startSim() {

	if (kow.flags.game_started) {
		alert('A simulation is in progress already!');
		return;
	}

	//Show the status area
	$('.status_area').fadeIn(250);

	//Show the status area
	$('.battlefield').removeClass('full_width');

	//We reset the games ran counter here and set the max number of games too
	kow.games_ran = 1;
	kow.total_games = $('#number_simulations').val();

	//Restart the game stats
	for(var i = 1; i <= kow.total_units; i++) {
		kow.stats[i] = {
			'games_won': 0,
			'games_won_first_charge': 0,
			'routes': 0,
			'hits_made': 0,
			'hits_dealt': 0,
			'damage_taken': 0,
			'damage_done': 0,
			'wavered_total': 0,
			'inspired_total': 0,
		}
	}
	kow.stats['turns'] = 0;
	kow.stats['games_ran'] = 0;
	kow.stats['games_ended_at'] = {};
	kow.stats['dice_rolls'] = {};
	for(var i = 1; i <= 6; i++) {
		kow.stats['dice_rolls'][i] = 0;
	}

	//Do anything that needs to be done for each and every game
	initGame();

	//Check to make sure that 2 units without ranged ability aren't attacking each other in a ranged dual. If they cannot attack, alert the user and switch to melee!
	if (kow.units[1].ranged == '-' &&  kow.units[2].ranged == '-') {
		reportStatus('Neither side can do ranged attacks! Switching to melee combat instead!', true, '');
		if (kow.mode == 'ranged_sim') kow.mode = 'melee_sim';
		if (kow.mode == 'single_ranged_sim') kow.mode = 'single_melee_sim';
		if (kow.mode == 'double_ranged_sim') kow.mode = 'double_melee_sim';
		$('#game_mode').val(kow.mode);
	}

	//Make sure that they units are in their proper positions...
	positionUnits();

	//Do the first turn
	doCombatTurn();

	//Hide the game settings for now
	$('#game_settings').slideUp(250);

	//Do the same for the unit spinners
	$('.unit_spinner').fadeOut(250);
}

//Game over function that cleans up after the game
function gameOver(forced_exit) {

	//Output the kow variable to the console
	console.log(kow);

	//Doing a forced exit or not?
	if (!forced_exit) {
		reportStatus('Game Over!', true, '');
	} else {
		reportStatus('Combat Simulation Over!', true, '');
	}

	//Do the flags and unit id
	kow.flags.game_started = false;
	kow.flags.game_over = false;

	//Make sure that the next game will begin with unit 1
	kow.unit = 1;

	//Increment the number of games ran
	kow.games_ran++;

	//Record which turn it ended on.
	//We devide by 2 to get the real turn number
	var real_turn = Math.round(kow.turn / 2);
	if (typeof kow.stats['games_ended_at'][real_turn] == 'undefined') {
		kow.stats['games_ended_at'][real_turn] = {};
		kow.stats['games_ended_at'][real_turn]['total'] = 0;
		kow.stats['games_ended_at'][real_turn]['winner_1'] = 0;
		kow.stats['games_ended_at'][real_turn]['winner_2'] = 0;
	}
	kow.stats['games_ended_at'][real_turn]['total']++;

	if (kow.units[1].routed) {
		kow.stats['games_ended_at'][real_turn]['winner_2']++;
	}
	if (kow.units[2].routed) {
		kow.stats['games_ended_at'][real_turn]['winner_1']++;
	}

	//Update the stats from this game
	collectMultiGameStats();

	//Are we below the magic 25 game threshold?
	if (kow.games_ran <= kow.total_games) {

		//Init the game again
		initGame();
		$('#game_progress_bar').css('width', calcPercentage(kow.total_games, kow.games_ran) + '%');
		$('#game_progress_text').html(kow.games_ran + ' of ' + kow.total_games + ' games ran.')

		//start the first turn!
		setTimeout(function() {
			doCombatTurn();
		}, 10);

	} else {

		//Are we doing more than 1 simulation?
		if (kow.total_games > 1) {

			//We clear the stat screen on the game 
			$('#status_panel').html('');

			//Output to the user!
			showGameStatSummaryScreen();
		}

		//update the gui
		setTimeout(function() {
			gameOverGUIChanges();
		}, 2000);
	}
}

function collectMultiGameStats() {
	var units = kow.units;
	if (units[1].routed) {
		kow.stats[1].routes++;
		kow.stats[2].games_won++;
		if (kow.turn == 2) kow.stats[2].games_won_first_charge++;
	}
	if (units[2].routed) {
		kow.stats[2].routes++;
		kow.stats[1].games_won++;
		if (kow.turn == 1) kow.stats[1].games_won_first_charge++;
	}
	
	for(var i = 1; i <= kow.total_units; i++) {
		kow.stats[i].hits_made += parseInt(units[i].attacks_made);
		kow.stats[i].hits_dealt += parseInt(units[i].hits_dealt);
		kow.stats[i].damage_taken += parseInt(units[i].current_damage);
		kow.stats[i].damage_done += parseInt(units[invertPlayer(i)].current_damage);
		kow.stats[i].wavered_total += parseInt(units[i].wavered_total);
		kow.stats[i].inspired_total += parseInt(units[i].inspired_total);
	}

	kow.stats['turns'] += kow.turn;
	kow.stats['games_ran']++;

	collectDiceStats();
}

//Swaps the currently active player
function swapPlayer() {

	kow.unit = invertPlayer(kow.unit);

	kow.turn++;

	reportStatus('It is ' + kow.units[kow.unit].unit_name + '\'s turn to act!', false, '080');
}

//Does the pre-game initialistation and clean up (if any)
function initGame() {

	//Set a couple of flags
	kow.flags.game_over = false;
	kow.flags.game_started = true;

	//Disable the combat fields
	enableDisableUnitEditor(1, true, true);
	enableDisableUnitEditor(2, true, true);

	//Reset the status log
	$('#status_panel').html('');

	//Reset the game object
	kow.mode = $('#game_mode').val();
	kow.display_mode = $('#result_display').val();
	kow.hindered_charges = $('#hindered_charges').val();
	kow.unit = 1;
	kow.turn = 1;
	kow.units = [];
	kow.rolls = [];

	//Game units
	for(var i = 1; i <= kow.total_units; i++) {
		kow.units[i] = {
			'unit_name': $('#unit_' + i +'_name').val(),
			'melee': $('#unit_' + i +'_melee').val(),
			'damage': $('#unit_' + i +'_damage').val(),
			'ranged': $('#unit_' + i +'_ranged').val(),
			'waver': $('#unit_' + i +'_waver').val(),
			'rout': $('#unit_' + i +'_rout').val(),
			'attacks': $('#unit_' + i +'_attacks').val(),
			'hits_dealt': 0,
			'attacks_made': 0,
			'current_damage': 0,
			'wavered_total': 0,
			'inspired_total': 0,
			'wavered': false,
			'routed': false,
			'is_counter_charge': false,
			'special_rules': {}
		}
	}

	//Cycle through all the special rules and update them
	var l = special_rules.length;
	for(var i = 0; i < l; i++) {
		kow.units[1].special_rules[special_rules[i]] = 0;
		kow.units[2].special_rules[special_rules[i]] = 0;

		if ($('#rule_' + special_rules[i] + '_1')[0].checked) kow.units[1].special_rules[special_rules[i]] = 1;
		if ($('#rule_' + special_rules[i] + '_2')[0].checked) kow.units[2].special_rules[special_rules[i]] = 1;
	}

	//Show the bars on screen for the non-inatant game mode...
	if (kow.display_mode != 'instantly') {
		$('#morale_1').slideDown(250);
		$('#morale_2').slideDown(250);
	}

	if (kow.total_games > 1) {

		$('#summary_screen_contents').html('<div class="damage_outer"><div class="damage_bar nerve_bar" id="game_progress_bar"></div></div><p class="c" id="game_progress_text"></p>');
		$('#stat_screen').fadeIn(250);
		showDarkener();
	}
}

//Core function that is called once the JS has loaded... Sets up the entire screen
function engineInit() {

	//Build the predefined unit selects elements
	buildPredefinedUnits();

	//Built the special rule areas
	buildSpecialRules();

	//Set up the units on the battlefield
	positionUnits();

	//attach all the events we have
	attachAllEvents();

	//Are we testing a specific feature?
	testingInit();

	if ($(document).width() < kow.battlefield_hide_threshold) {
		toggleBattlefield();
	}

	$('#latest_update').html(kow.latest_update);
	$('#version_number').html(kow.version);
}

/*
	Collects all the dice stats and compiles them into a single object to use later
*/
function collectDiceStats() {
	var l = kow.rolls.length;
	for(var i = 0; i < l; i++) {
		kow.stats['dice_rolls'][kow.rolls[i]]++;
	}
}