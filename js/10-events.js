function attachAllEvents() {
	$('#enable_testing').on('click', function() {
		if (window.confirm("Do you really want to enable testing mode?")) {
			window.location.hash = '#testing';
			testingInit();
		}
	});
	$('#number_simulations').on('change', function() {
		if ($(this).val() >= 25) {
			$('#result_display').val('instantly');
		}
	});

	$('.swap_units').on('click', function() {
		swapUnits();
	});

	$('.close_button, #darkener, .dialog h3').on('click', function() {
		hideDialogs();
	});

	//Unit spinners
	$('.unit_spinner .button').on('click', function() {
		swapFacing($(this).data('unit'), $(this).data('direction'));
	});

	$('#start_sim').on('click', function() {
		startSim();
	});

	$('.unit_name_field').on('change', function() {
		var unit_id = $(this).data('unit');
		$('#main_unit_' + unit_id).html($(this).val());
	});

	//Unit presents	
	$('#unit_1_presets, #unit_2_presets').on('change', function() {
		handleUnitPreset($(this).data('unit'), $(this).val());
	});

	//Reveal rules?
	$('.reveal_rules').on('click', function() {
		$('#unit_rules_1, #unit_rules_2').slideToggle(500);
	});

	//Dice value fields
	$('.val_dice_value').on('change', function() {
		handleValidation(2, 2, 6, false, $(this));
	});

	//Numeric value fields
	$('.val_numeric').on('change', function() {
		handleValidation(10, 0, 0, false, $(this));
	});

	//Dice that can also be dashed value fields
	$('.val_dice_value_dashed').on('change', function() {
		handleValidation(2, 2, 6, true, $(this));
	});

	//Numeric value dashed
	$('.val_num_value_dashed').on('change', function() {
		handleValidation(11, 0, 0, true, $(this));
	});

	//Search special rule boxes
	$('.special_rule_search input').on('focus', function() {

		$('#unit_rules_1, #unit_rules_2').slideDown(500);

		//Over search flag set
		kow.flags.over_search = $(this).data('unit');

		searchBoxTimer();

	}).on('blur', function() {

		//Set the over search flag to false
		kow.flags.over_search = false;

		//Check the search box values - if they are empty show all the special rules
		if ($('#search_box_1').val() == '') $('#unit_rules_1 p').slideDown(250);
		if ($('#search_box_2').val() == '') $('#unit_rules_2 p').slideDown(250);
	});
	$('.battlefield legend, .status_area legend').on('click', function() {
		toggleBattlefield(false);
	});
}

function handleValidation(default_value, min_value, max_value, allow_dash, ref) {

	//Get the value for the referenced item passed in
	var val = ref.val();

	//Are we allowing a dash?
	if (allow_dash && val == '-') return;

	//Is not a valid number?
	if (isNaN(val) && val == '') {
		ref.val(default_value);
		return;
	}

	//Between the min & max values?
	if (min_value != 0 && ref.val() < min_value) ref.val(min_value);
	if (max_value != 0 && ref.val() > max_value) ref.val(max_value);
}

function handleUnitPreset(unit_select, unit_id) {

	//Is the value -1? Because that means we are not selecting a specific unit!
	if (unit_id == -1) {
		enableDisableUnitEditor(unit_select, false, false);
		return;
	}

	//Setting the values to the respective new unit
	var unit = unit_def[unit_id];
	$('#unit_' + unit_select + '_attacks').val(unit.attacks);
	$('#unit_' + unit_select + '_damage').val(unit.damage);
	$('#unit_' + unit_select + '_waver').val(unit.waver);
	$('#unit_' + unit_select + '_melee').val(unit.melee);
	$('#unit_' + unit_select + '_ranged').val(unit.ranged);
	$('#unit_' + unit_select + '_rout').val(unit.rout);

    //Is the unit name Unit X? Set it to the unit name here if
	if (
		$('#unit_' + unit_select + '_name').val() == 'Unit ' + unit_select || 
		$('#unit_' + unit_select + '_name').val() == ''
	) {
		var name = unit_def[$('#unit_' + unit_select + '_presets').val()].unit_name;
		if (name.indexOf(' [')) name = name.substr(0, name.indexOf(' ['));
		$('#unit_' + unit_select + '_name').val(name);
		$('#unit_' + unit_select + '_name').trigger('change');
	};

	//Cycle through the unit special rules
	var l = special_rules.length;
	for(var i = 0; i < l; i++) {

		//Disabling the special rules
		$('#rule_' + special_rules[i] + '_' + unit_select)[0].checked = false;

		//Enabling any special rules for the unit
		var l2 = unit.special_rules.length;
		for(var t = 0; t < l2; t++) {
			$('#rule_' + unit.special_rules[t] + '_' + unit_select)[0].checked = true;
		}
		
	}

	//Disable the combat values so that they are not editable
	enableDisableUnitEditor(unit_select, true, false);
}

function handleWindowResize() {

	if (kow.flags.resize_debouncer) {
		clearTimeout(kow.flags.resize_debouncer);
	}

	kow.flags.resize_debouncer = setTimeout(function() {

		//Add all the functions to handle on resize - they are called only once!
        toggleBattlefield(true);
		positionUnits();
    }, 500);
}

//On resize, we need to re-position the units on the screen
window.onresize = handleWindowResize;

//Actually start the game engine
window.onload = engineInit;