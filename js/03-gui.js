/*
	Updates the morale bars for the gui...
	@param unit_id int - The unit id that is currently in focus
	@param nerve int - the result of the nerve roll
	@param extra_damage int - However additional damage this attack did to the enemy unit
*/
function updateMoraleBars(unit_id, nerve, extra_damage) {

	var unit_1 = kow.units[1].current_damage;
	var unit_2 = kow.units[2].current_damage;
	unit_1_nerve = kow.units[1].current_damage;
	unit_2_nerve = kow.units[2].current_damage;

	//These are inverted values - it is damage being applied by one unit to the other unit
	if (unit_id == '1') {
		unit_2 += extra_damage;
		unit_2_nerve = unit_2 + nerve;
	}
	if (unit_id == '2') {
		unit_1 += extra_damage;
		unit_1_nerve = unit_1 + nerve;
	}

	//Calculates the target values for the moral bars
	$('#unit_1_damage_bar').css('width', calcPercentage(kow.units[1].rout, unit_1) + '%');
	$('#unit_2_damage_bar').css('width', calcPercentage(kow.units[2].rout, unit_2) + '%');

	$('#unit_1_nerve_bar').css('width', calcPercentage(kow.units[1].rout, unit_1_nerve) + '%');
	$('#unit_2_nerve_bar').css('width', calcPercentage(kow.units[2].rout, unit_2_nerve) + '%');

	$('#unit_1_morale_bar').css('width', calcPercentage(kow.units[1].rout, (unit_1 + 12)) + '%');
	$('#unit_2_morale_bar').css('width', calcPercentage(kow.units[2].rout, (unit_2 + 12)) + '%');

	$('#unit_1_morale_score').html ('<strong>Current Damage:</strong> ' + unit_1 + ' / ' + kow.units[1].rout);
	$('#unit_2_morale_score').html ('<strong>Current Damage:</strong> ' + unit_2 + ' / ' + kow.units[2].rout);
}

/*
	Search box timer that runs to handle searching for specific special rule
*/
function searchBoxTimer() {

	//is the over_search flag global set?
	if (kow.flags.over_search) {

		var search_for = $('#search_box_' + kow.flags.over_search).val();
		search_for = search_for.toLowerCase();

		if (search_for != '') {

			//Loop through all the rules for the unit to check against each one
			$('#unit_rules_' + kow.flags.over_search + ' p').each(function() {

				var rule = ' ' + $(this).data('rule') + ' ';

				if (rule.indexOf(search_for) > 0) {

					$(this).slideDown(250);

				} else {

					$(this).slideUp(250);

				}
			});
			/*
			Note we don't need to show all the records if the search term is done.
			this is done once be the onblur function of the search box
			*/
		}

		setTimeout('searchBoxTimer()', 500);
	}
}

/*
	Adds a line of text to the status panel - tags the line, bold (bool) & color (string, '' for no color)
	@param status string - The status text to appear in the console.
	@param bold bool - Whether to make it bold or not?
	@param color string - A hex-dec colour value - or '' for no special colour
*/
function reportStatus(status, bold, color) {

	if (bold) status = '<b>' + status + '</b>';

	var html = '<p';

	if (color != '') html += ' style="color:#' + color + '"';

	html += '>' + status + '</p>';

	$('#status_panel').prepend(html);
}

/*
	Builds the special rules panels for us
*/
function buildSpecialRules() {

	var l = special_rules.length;

	var html = '';

	//Loop through the special rules
	for(var i = 0; i < l; i++) {

		//Set a temp id as it is used twice below
		var id = 'rule_' + special_rules[i] + '_#';

		html += '<p data-rule="' + special_rules[i] + '""><label for="' + id + '">' + parseSpecialRule(special_rules[i]) + ':</label><input type="checkbox" value="1" data-special-rule="' + special_rules[i] + '" id="' + id + '" name="' + id + '" class="special_rule_selector special_rules_#" /></p>';
	}

	html += '<br class="clear" />';

	$('#unit_rules_1').html(html.replace(/#/g, '1'));
	$('#unit_rules_2').html(html.replace(/#/g, '2'));
}

/*
	Parses the special rule titles for us - as they are lower case
	@param rule string - The name of the rule
	@return rule string - The modified name of the rule's name
*/
function parseSpecialRule(rule) {

	rule = rule + '';

	//Make the words capitalised
	rule = ucWords(rule);

	rule = rule.replace(/_/g, ' ');

	// (n+) special rule endings (regenerate n+)
	rule = rule.replace('--5', ' (5+)');
	rule = rule.replace('--6', ' (6+)');
	rule = rule.replace('--4', ' (4+)');
	rule = rule.replace('--3', ' (3+)');
	rule = rule.replace('--2', ' (2+)');

	// (n) special rule endings (piercing (n))
	for(var i = 10; i >= 0; i--) {
		rule = rule.replace('-' + i, ' (' + i + ')');
	}	

	return rule;
}

/*
	Builds the predefined unit selects options

*/
function buildPredefinedUnits() {

	var l = unit_def.length;

	var current_army = -1;

	var html = '';

	for(var i = 0; i < l; i++) {

		//Have we switched armies? - we need to set up an opt group for them
		if (current_army != unit_def[i].army) {

			html += '</optgroup><optgroup label="' + armies[unit_def[i].army] + '">';

			current_army = unit_def[i].army;
		}

		html += '<option value="' + i + '">' + unit_def[i].unit_name + '</option>';
	}

	//Add the select preset unit option, remove the first <optgroup> and add the last closing </optgroup>
	html = '<option value="-1">Select Preset Unit</option>' + html.substr(11, html.length) + '</optgroup>';

	$('#unit_1_presets').html(html);
	$('#unit_2_presets').html(html);
}

/*
	Helper function that disables all the user fields such as Melee, range, unit name etc
	@param unit_select number - Which unit to apply it too?
	@param disabled bool - whether to disable or enable the fields
	@param disable_non_stat_fields bool - Disable the name/ pre-defined selects
*/
function enableDisableUnitEditor(unit_select, disabled, disable_non_stat_fields) {

	//possibly rewrite to use a class instead?
	$('#unit_' + unit_select + '_attacks')[0].disabled = disabled;
	$('#unit_' + unit_select + '_damage')[0].disabled = disabled;
	$('#unit_' + unit_select + '_waver')[0].disabled = disabled;
	$('#unit_' + unit_select + '_melee')[0].disabled = disabled;
	$('#unit_' + unit_select + '_rout')[0].disabled = disabled;
	$('#unit_' + unit_select + '_ranged')[0].disabled = disabled;

	//Loop through the special rules now and disable them...
	var l = special_rules.length;
	for(var i = 0; i < l; i++) {
		$('#rule_' + special_rules[i] + '_' + unit_select)[0].disabled = disabled;
	}

	//Are we disabling the selects & unit name too?
	if (disable_non_stat_fields) {
		$('.swap_units').fadeOut(250);
		$('#unit_' + unit_select + '_presets')[0].disabled = disabled;
		$('#unit_' + unit_select + '_name')[0].disabled = disabled;
	} else {
		$('.swap_units').fadeIn(250);
	}
}
/*
	Calculates the offsets for each unit
	@return offset int - Number of pixels for each offset area
*/
function calculateUnitOffset() {
	return ($('.battlefield').width() - $('#main_unit_1').width() - $('#main_unit_2').width()) / 4;	
}

/*
	Keeps the units in their assigned place - dimension independent
*/
function positionUnits() {

	//If we have a routed unit - we need to make sure that it doesn't come back - if we let this continue it will appear back on the battlefield
	if (kow.flags.game_over) return;

	//Calc the offset using our helper function
	var offset = calculateUnitOffset();

	$('#main_unit_1').css('left', offset).css('transform', 'rotate(' + kow.unit_facing_real[1] + 'deg)');
	$('#main_unit_2').css('left', (offset * 3) + $('#main_unit_1').width()).css('transform', 'rotate(' + kow.unit_facing_real[2] + 'deg)');

}
/*
	Moves the attacking unit into contact
	@param unit_id int - which unit is the focus of the event?
*/
function moveAttackingUnit(unit_id) {

	var offset = calculateUnitOffset();

	if (unit_id == '1') {
		var left = (offset * 3);
		var combat_left = left + 100;
	}
	if (unit_id == '2') {
		var left = (offset + $('#main_unit_1').width());
		var combat_left = left - 100;
	}
	$('.combat_overlay').css('left', combat_left);
	$('#main_unit_' + unit_id).css('left', left);
	
	setTimeout(function() {
		$('.combat_overlay').fadeIn(250, function() {
			$('.combat_overlay').fadeOut(500);
		});
	}, 1000);
}
/*
	Animates the routed unit off the battlefield
	@param unit_id int - which unit is the focus of the event?
*/
function animateRoutedUnit(unit_id) {

	if (unit_id == '1') var left = (0 - $('#main_unit_1').width()) * 2;
	if (unit_id == '2') var left = $('.battlefield').width() * 2;

	$('#main_unit_' + unit_id).css('left', left);
}
/*
	Runs the ranged combat animation
	@param unit_id int - which unit is the focus of the event?
*/
function animateRangedCombat(unit_id) {
	if (unit_id == '1') {
		var pos_1 = $('#main_unit_1').offset();
		var pos_2 = $('#main_unit_2').offset();
		pos_1 = pos_1.left - 10;
		pos_2 = pos_2.left + 192;
		var animate_left = pos_2 - pos_1;
	} else {
		var pos_1 = $('#main_unit_2').offset();
		var pos_2 = $('#main_unit_1').offset();
		pos_1 = pos_1.left + 150;
		pos_2 = pos_2.left;
		var animate_left = pos_2;
	}
	
	$('#ranged_combat').css('left', pos_1).fadeIn(100).animate({'left': animate_left}, 3000).fadeOut(100);
}

/*
	Moves the unit up and down to denote that it is wavered
	@param unit_id int - which unit is the focus of the event?
*/
function animateWaveredUnit(unit_id) {
	/*
	$('#main_unit_' + unit_id).css('top', 30);
	setTimeout(function() {
		$('#main_unit_' + unit_id).css('top', 60);
	}, 500);
	setTimeout(function() {
		$('#main_unit_' + unit_id).css('top', 45);
	}, 1500);
*/
}

/*
	Function that handles all the time outs for the animated battlefield - you pass it an event
	@param event_name string - denotes the name of the event
	@param unit_id int - which unit is the focus of the event?
*/
function animateBattlefield(event_name, unit_id) {

	//If the battlefield area is hidden, we cannot really do much... so we return
	if (kow.flags.battlefield_hidden) return;
	if (kow.display_mode == 'instantly') return;

	//Unit has fired!
	if (event_name == 'ranged_attack') {
		animateRangedCombat(unit_id);
		return;
	}

	//Unit has been wavered!
	if (event_name == 'unit_wavered') {
		animateWaveredUnit(unit_id);
		return;
	}

	//Move attacking unit
	if (event_name == 'move_attack') {

		//Update the unit position.
		positionUnits();

		//Add the charge! command!
		setTimeout(function() {
			showBattlefieldMessage('Charge!', unit_id);
		}, 250);

		//Move them now!
		moveAttackingUnit(unit_id);

		//Return them to their positions after 2s
		setTimeout(function() {
			positionUnits();
		}, 2000);
		return;
	}

	//Turning the face the enemy!
	if (event_name == 'turn_to_face_enemy') {
		showBattlefieldMessage('Face Enemy!', unit_id);
		positionUnits();
		return;
	}

	//Enemy routed!
	if (event_name == 'enemy_routed') {
		setTimeout(function() {
			showBattlefieldMessage('Routed!', unit_id);
			animateRoutedUnit(unit_id);
		}, 1500);
		return;
	}
}

/*
	Toggles the battlefield view on and off
	@param resize_event bool - whether it was a resize event that triggered it or not
*/
function toggleBattlefield(resize_event) {

	//If the screen is smaller than the battlefield_hide_threshold, hide the battlefield...
	if ($(document).width() <= kow.battlefield_hide_threshold) {

		//Note: we don't set a flag here as we want the battlfield to return if the user has elected for it do so.
		$('.status_area, .battlefield').addClass('battlefield_hidden');
		return;
	}

	//Is this coming from a resize event? Well we are not really toggling the battlefield view but maybe just showing it?
	if (resize_event) {

		//Showing it or not?
		if (kow.flags.battlefield_hidden) {
			hideBattlefield();
		} else {
			showBattlefield();
		}

		//Need to return here otherwise it will just toggle...
		return;
	}

	//Actually doing a toggle command?
	if (!kow.flags.battlefield_hidden) {
		hideBattlefield();
	} else {
		showBattlefield();
	}
}

/*
	Hide the battlefield area and expand the status area.
*/
function hideBattlefield() {
	kow.flags.battlefield_hidden = true;
	$('.status_area, .battlefield').addClass('battlefield_hidden');
}

/*
	Show the battlefield area and reduce the status area.
*/
function showBattlefield() {
	kow.flags.battlefield_hidden = false;
	$('.status_area, .battlefield').removeClass('battlefield_hidden');
}

/*
	Shows a nice battlefield message on the battlefield screen
	@param message string - Text to show on the battlefield
	@param unit_id int - The unit to apply it too.
*/
function showBattlefieldMessage(message, unit_id) {

	//If they are resolving it instantly, don't show any messages!
	if (kow.display_mode == 'instantly') return;

	//Calculate the pos
	var pos = $('#main_unit_' + unit_id).offset();
	pos.top -= 35;
	pos.left -= 10;
	
	//Append it to the battlefield in the starting position
	$('.battlefield').append('<div class="battlefield_message" style="top:' + pos.top + 'px;left:' + pos.left + 'px">' + message + '</div>');

	//Animate it vertically and the remove it
	$('.battlefield_message').animate({'top': 0, 'opacity': 0}, 3000, function() {
		$(this).remove();
	});
}

/*
	Handles the game over changes for the GUI
*/
function gameOverGUIChanges() {
	//Show the hidden game elements
	$('#game_settings').slideDown(250);
	$('.unit_spinner').fadeIn(250);	

	//Re-enable the combat fields
	enableDisableUnitEditor(1, false, true)
	enableDisableUnitEditor(2, false, true);

	//Hide the morale
	$('#morale_1').slideUp(250);
	$('#morale_2').slideUp(250);

	//Trigger an onchange event on the unit preset so that we can make changes to the units
	$('#unit_1_presets').trigger('change');
	$('#unit_2_presets').trigger('change');

	positionUnits();
}

/*
	Shows a nice popup for the user to appreciate the awesomeness of their units!
*/
function showGameStatSummaryScreen() {
	var stats = kow.stats;
	var html = '';
	html += '<div class="half_col">';
	html += '<h4>General Game Stats</h4>';
	html += '<p>Games Played: ' + numberformat(stats['games_ran']) + '</p>';
	html += '<p>Total Turns Played: ' + numberformat(Math.round(stats['turns'] / 2)) + '</p>';
	html += '<p>Total Combat Rounds Played: ' + numberformat(stats['turns']) + '</p>';

	html += '</div>';
	html += '<div class="half_col">';
	html += '<h4>Average Combat Length</h4>';
	html += '<table>';
	html += '<tr><th>Turns</th><th colspan="2">Games Ended</th></tr>';
	var max_turns = 0;
	for (var key in kow.stats['games_ended_at']) {
		var turns = kow.stats['games_ended_at'][key];
		if (max_turns < turns.total) max_turns = turns.total;
	}

	for (var key in kow.stats['games_ended_at']) {
		if (!kow.stats['games_ended_at'].hasOwnProperty(key)) continue;
		var turns_obj = kow.stats['games_ended_at'][key];

		html += '<tr>';
			html += '<th>' + key + '</th>';
			html += '<td class="r">' + turns_obj.total + '</td>';
			html += '<td class="bar_col">';
				html += '<div class="bar" title="Total Battles" style="width: ' + calcPercentage(max_turns, turns_obj.total) + '%"></div>';
				if (turns_obj.winner_1 > 0) html += '<div class="bar bar_unit_1" title="Unit 1 : ' + kow.units[1].unit_name + '" style="width: ' + calcPercentage(max_turns, turns_obj.winner_1) + '%"></div>';
				if (turns_obj.winner_2 > 0) html += '<div class="bar bar_unit_2" title="Unit 2 : ' + kow.units[2].unit_name + '" style="width: ' + calcPercentage(max_turns, turns_obj.winner_2) + '%"></div>';
			html += '</td>';
		html += '</tr>';
	}
	html += '</table>';
	html += '<div class="bar small_text c fl" title="Total Battles" style="width: 33.3333%;height: auto">Total Battles</div>';
	html += '<div class="bar small_text c bar_unit_1" title="Unit 1 : ' + kow.units[1].unit_name + '" style="width: 33.3333%;height: auto">Unit 1 : ' + kow.units[1].unit_name + '</div>';
	html += '<div class="bar small_text c bar_unit_2" title="Unit 2 : ' + kow.units[2].unit_name + '" style="width: 33.3333%;height: auto">Unit 2 : ' + kow.units[2].unit_name + '</div>';
	html += '<br class="clear" />';
	html += '</div>';
	html += '<br class="clear" />';

	for(var i = 1; i <= kow.total_units; i++) {
		html += '<div class="half_col">';
		html += '<h4>Unit ' + i + ' - ' + kow.units[i].unit_name  + '</h4>';
		html += '<p>Games Won: ' + numberformat(stats[i].games_won) + ' (' + numberformat(calcPercentage(stats['games_ran'], stats[i].games_won)) + '%)</p>';
		html += '<p>Games Won (first charge): ' + numberformat(stats[i].games_won_first_charge) + ' (' + numberformat(calcPercentage(stats['games_ran'], stats[i].games_won_first_charge)) + '%)</p>';
		html += '<p>Attacks Made: ' + numberformat(stats[i].hits_made) + '</p>';
		html += '<p>No of Attacks that Hit: ' + numberformat(stats[i].hits_dealt) + '</p>';
		html += '<p>%age of Attacks Hit: ' + numberformat(calcPercentage(stats[i].hits_made, stats[i].hits_dealt)) + '%</p>';
		html += '<p>Damage Taken: ' + numberformat(stats[i].damage_taken) + '</p>';
		html += '<p>Damage Given: ' + numberformat(stats[i].damage_done) + '</p>';
		html += '<p>Times Wavered: ' + numberformat(stats[i].wavered_total) + '</p>';
		html += '<p>Times Inspired: ' + numberformat(stats[i].inspired_total) + '</p>';
		html += '<p>Damage Taken/Given Ratio: 1 : ' + numberformat(stats[i].damage_taken / stats[i].damage_done) + '</p>';
		html += '<p><strong>Averages</strong></p>';
		html += '<p>Average Hits/Game: ' + numberformat(stats[i].hits_dealt / stats['games_ran']) + '</p>';
		html += '<p>Average Damage Given/Game: ' + numberformat(stats[i].damage_done / stats['games_ran']) + '</p>';
		html += '<p>Average Damage Taken/Game: ' + numberformat(stats[i].damage_taken / stats['games_ran']) + '</p>';
		html += '<p>Average Times Wavered Per Game: ' + numberformat(stats[i].wavered_total / stats['games_ran']) + '</p>';
		html += '</div>';
	}
	html += '<br class="clear" />';


	var max_dice_result = 0;
	var total_dice_rolled = 0;
	for(var i = 1; i <= 6; i++) {
		if (max_dice_result < kow.stats['dice_rolls'][i]) max_dice_result = kow.stats['dice_rolls'][i];
		total_dice_rolled += kow.stats['dice_rolls'][i];
	}

	html += '<div class="half_col"><h4>Dice Results</h4>';
		html += '<table>';
		html += '<tr><th>Dice Result</th><th colspan="2">Number of Results Rolled</th></tr>';
		for(var i = 1;i <= 6; i++) {
			html += '<tr><th>' + i + '</th><td>' + kow.stats['dice_rolls'][i] + '</td>';
			html += '<td class="bar_col"><div class="bar" title="Total results for ' + i + '" style="width: ' + calcPercentage(max_dice_result, kow.stats['dice_rolls'][i]) + '%"></div></td>';
			html += '</tr>';
		}
	
		html += '<tr><th colspan="2" class="r">Total Dice Rolled: </th><th class="l">' + numberformat(total_dice_rolled) + '</th></tr></table></div>';
	html += '<br class="clear" />';
	var h = window.innerHeight;
	h -= 120;
	$('#summary_screen_contents').html(html).css('max-height', h);


	showDarkener();
	$('#stat_screen').fadeIn(250);
}

/*
	Shows a darkener to highlight a popup
*/
function showDarkener() {
	$('#darkener').fadeIn(250);
}

/*
	Hides all the dialogs
*/
function hideDialogs() {
	$('.dialog').fadeOut(250);
	$('#darkener').fadeOut(250);
}

/*
	Takes a unit and a direction and updates the direction
*/
function swapFacing(unit, direction) {

	if (kow.flags.animating_unit_rotation) return;
	kow.flags.animating_unit_rotation = true;
	$('.unit_spinner').css('opacity', 0.4);

	var current_facing = kow.unit_facing[unit];
	var new_facing = 't';

	if (direction == 'cc') {
		if (current_facing == 'l') new_facing = 'b';
		if (current_facing == 'b') new_facing = 'r';
		if (current_facing == 'r') new_facing = 't';
		if (current_facing == 't') new_facing = 'l';
		kow.unit_facing_real[unit] -= 90;
	} else {
		if (current_facing == 'l') new_facing = 't';
		if (current_facing == 'b') new_facing = 'l';
		if (current_facing == 'r') new_facing = 'b';
		if (current_facing == 't') new_facing = 'r';
		kow.unit_facing_real[unit] += 90;
	}

	//Udate the unit facing variable
	kow.unit_facing[unit] = new_facing;

	positionUnits();
	setTimeout(function() {
		kow.flags.animating_unit_rotation = false;
		$('.unit_spinner').css('opacity', 1);
	}, 1000);
}