//Holds all the special rules we currently support
var special_rules = ['big-shield', 'brutal', 'crushing_strength-1', 'crushing_strength-2', 'crushing_strength-3', 'crushing_strength-4', 'crushing_strength-5', 'elite', 'ensnare', 'fury', 'headstrong', 'individual', 'inspiring', 'iron-resolve', 'lifeleech-1', 'lifeleech-2', 'lifeleech-3', 'lifeleech-4', 'lifeleech-5', 'lifeleech-6', 'lifeleech-7', 'lifeleech-8', 'lifeleech-9', 'lifeleech-10', 'pathfinder', 'phalanx', 'piercing-1', 'piercing-2', 'piercing-3', 'piercing-4', 'piercing-5', 'regeneration--2', 'regeneration--3', 'regeneration--4', 'regeneration--5', 'regeneration--6', 'stealthy', 'strider', 'thunderous_charge-1', 'thunderous_charge-2', 'thunderous_charge-3', 'thunderous_charge-4', 'thunderous_charge-5', 'yellow_bellied', 'vicious'];

//kow var, morale bar positions and targets 
var kow = {
	mode: $('#game_mode').val(),
	display_mode: $('#result_display').val(),
	unit: 1,
	turn: 1,
	routs_total: 0,
	stats: [],
	units: [],
	rolls: []
};
kow.battlefield_hide_threshold = 980;
kow.hindered_charges = 0;
kow.unit_facing = [];
kow.unit_facing_real = [];
kow.unit_facing[1] = 'r';
kow.unit_facing[2] = 'l';
kow.unit_facing_real[1] = '-90';
kow.unit_facing_real[2] = '90';

kow.total_units = 2;
kow.unit_on_which_side = [];
kow.unit_on_which_side[1] = 1;
kow.unit_on_which_side[2] = 2;

//Flags
kow.flags = {game_started: false, over_search: false, game_over: false, battlefield_hidden: false, resize_debouncer: false, animating_unit_rotation: false};

//Number of games that have been ran
kow.games_ran = 0;
kow.total_games = 0;

//Engine information
kow.version = 0.9;
kow.latest_update = '17/03/2017';